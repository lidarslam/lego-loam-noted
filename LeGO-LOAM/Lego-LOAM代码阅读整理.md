# Lego-LOAM代码阅读整理

### Part1：文件目录结构

LeGO-LOAM：

├─1.cloud_msgs
│  │  CMakeLists.txt
│  │  package.xml
│  └─msg
│          cloud_info.msg
└─2.LeGO-LOAM
    │  CMakeLists.txt
    │  package.xml
    ├─include
    │      utility.h
    ├─launch
    │      block.png
    │      dataset-demo.gif
    │      demo.gif
    │      google-earth.png
    │      jackal-label.jpg
    │      odometry.jpg
    │      run.launch
    │      seg-total.jpg
    │      test.rviz
    └─src
            featureAssociation.cpp
            imageProjection.cpp
            mapOptmization.cpp
            transformFusion.cpp

### Part2：launch

launch文件包含四部分：

1.Sim Time

2.Rviz（可视化界面）

3.静态TF（lidar to map） （lidar to odometry）

### Part3：source

##### imageProjection.cpp：

一、初始化发接收，为点集分配空间，重置参数。

点集分为了laserCloudIn（按列排序），fullCloud（按行排列），fullInfoCloud（按行排列)

​                   groundCloud, segmentedCloud, segmentedCloudPure, outlierCloud

二、核心函数cloudHandler()

1.copyPointCloud()

将msg的数据拷贝到laserCloudIn，并去掉nan

（原始数据格式为sensor_msgs::PointCloud2，拷贝后为PointXZYI格式）

2.findStartEndAngle()  

计算起始角度与终止角度

3.projectPointCloud()  

将点阵转化为距离图像rangMat，并保存fullInfoCloud，按行排列

4.groundRemoval()   

 直接通过计算与水平面的夹角，小于等于10度就认为是地面，groundMat=1.

5.cloudSegmentation() 

首先使用labelComponents()对没有被标记地面特征的点进行分块，核心思想是判断两个测量的距离是否相近，如果最后统计点集数量大于30或者竖直方向大于5，则被认为是有效点集。

之后将离散点的五分之一加入到outlierCloud中，将地面点的五分之一加入到segmentedCloud，将所有的有效点集加入到segmentedCloud中。

这里segmentedCloud就不再是所有的点集了，那么为了保存原有点云的数据，采用了segMsg保存了ground，columnIndex和range信息。

6.publishCloud（）

发布了segMsg，outlierCloud，segmentedCloud这些后面会用

发布了fullCloud，groundCloud，segmentedCloudPure（无地面点），fullInfoCloud后面不用

##### featureAssociation.cpp：

接收了上一个cpp分割后的点，以及imu信息，初始化了相关变量

核心：runFeatureAssociation()

首先进行Feature Extraction

1.adjustDistortion





注：这里的lessflat点集包含了flat，surf同理



##### mapOptmization.cpp



##### transformFusion.cpp







GTSAM库：

